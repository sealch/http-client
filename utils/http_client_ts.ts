import fetch from "node-fetch";

async function postJson(url: string, obj: object) {
  const response = await fetch(url, {
    body: JSON.stringify(obj),
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    }
  });
  return await response.json();
}

async function putJson(url: string, obj: object) {
  const response = await fetch(url, {
    body: JSON.stringify(obj),
    method: "PUT",
    headers: {
      "Content-Type": "application/json"
    }
  });
  return await response.json();
}

async function deleteJson(url: string, obj: object) {
  const response = await fetch(url, {
    body: JSON.stringify(obj),
    method: "DELETE",
    headers: {
      "Content-Type": "application/json"
    }
  });
  return await response.json();
}

export async function getJson(url: string, params = {}) {
  let query = "?" + new URLSearchParams(params);
  if (!Object.keys(params).length) query = "";
  const response = await fetch(url + query, {
    method: "GET",
    headers: {
      "Content-Type": "application/json"
    }
  });
  return await response.json();
}

export default { postJson, getJson, putJson, deleteJson };
