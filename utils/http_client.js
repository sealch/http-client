const fetch = require('node-fetch');

async function postJson(url, obj) {
    let response = await fetch(url, {
        body: JSON.stringify(obj),
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        }
    });
    return await response.json();
}

async function putJson(url, obj) {
    let response = await fetch(url, {
        body: JSON.stringify(obj),
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        }
    });
    return await response.json();
}

async function deleteJson(url, obj) {
    let response = await fetch(url, {
        body: JSON.stringify(obj),
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
        }
    });
    return await response.json();
}

async function getJson(url, params = {}) {
    let query = '?' + new URLSearchParams(params);
    if (!Object.keys(params).length) query = '';
    let response = await fetch(url + query, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        }
    });
    return await response.json();
}

module.exports = { postJson, getJson, putJson, deleteJson };
